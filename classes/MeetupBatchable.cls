/**
 * Created by egraells on 31/05/18.
 */

/*
Meetup: vamos a crear 100 trabajos batch, veremos como transicionan de la Flex Queue a la
en estado Holding, a la cola Apex Jobs en los siguientes estados

List<egraells__Process_Log__c> pList = [SELECT Id from egraells__Process_Log__c];
delete pList;

for (integer i=0; i<99; i++) {
    egraells.MeetupBatchable mb = new egraells.MeetupBatchable();
    Database.executeBatch(mb);
}

//Se puede indicar el numero de registros que se quieren gestionar por cada job
//Por ejemplo si volvemos a lanzar, pero indicamos el valor de 1,
//tendremos tantos "duermetes" como accounts tengamos en el sistmema (sobre 57)
//Database.executeBatch(mb, 1);

Mostrar el Process Logs

 */



global with sharing class MeetupBatchable implements Database.Batchable<sObject>, Database.Stateful{
    //El uso de Stateful es opcional

    private Integer contadorStateful = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) {

        //Los registros que devolvemos como QueryLocator seran el Scope que recibe execute
        return Database.getQueryLocator('SELECT ID FROM Account ');
    }

    global void execute(Database.BatchableContext bc, List<sObject> listaObjetos) {
        // Teoricamente deberíamos trabajar con los registros de la variable Scope
        //pero realmente hacemos lo que necesitemos

        duermete (2* 1000);
        Logger.insertLogEntry('-ege- Contador Stateful: ' + ++contadorStateful);

    }

    global void finish(Database.BatchableContext bc) {

        AsyncApexJob jobInfo = [
                SELECT Id, Status, NumberOfErrors,
                        JobItemsProcessed,
                        TotalJobItems, CreatedBy.Email
                FROM AsyncApexJob
                WHERE Id = :bc.getJobId()
        ];

        Logger.insertLogEntry('-ege- Job: ' + jobInfo);

    }

    private void duermete(decimal milis) {

        Long startTimestamp = System.currentTimeMillis();
        Long timeDiff = 0;

        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milis);

        Logger.insertLogEntry('Me he echado un siesta de: ' + (System.currentTimeMillis() - startTimestamp) / 1000 + ' segundos');

    }
}