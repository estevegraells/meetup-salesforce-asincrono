/**
 * Created by egraells on 31/05/18.
 */

/*
Meetup:

MeetupSchedulable aDormir = new MeetupSchedulable();
// Seconds Minutes Hours Day_of_month Month Day_of_week(CHAR-?) optional_year

// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
for (integer i=0; i<60; i++){

    String sch = '0 ' + i + ' * * * ? *';
    String jobID = System.schedule('Dormitador'+i, sch, new MeetupSchedulable());

}


//En la vista de la Apex Job Queue no aparece la ejecución del Job,
//sinó que únicamente aparece planificado. La confirmación de la ejecución
//del Job aparece en la vista Scheduled, indicando la fecha de la última ejecución en
//la columna started.

// Para desplanificar todos los jobs scheduled:
//for(CronTrigger CT: [SELECT id from CronTrigger])
//{
//    try
//    {
//        System.abortjob(CT.id);
//    }catch(Exception e1)
//    {
//        System.debug('ups');
//    }
//}



 */



public with sharing class MeetupSchedulable implements Schedulable {

    public void execute(SchedulableContext ctx) {

        duermete(1000);
    }

    private void duermete(decimal milis) {

        Long startTimestamp = System.currentTimeMillis();
        Long timeDiff = 0;

        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milis);

        Logger.insertLogEntry('Mediante el Schedulable, me he echado un siesta de: ' + (System.currentTimeMillis() - startTimestamp) / 1000 + ' segundos');

    }
}