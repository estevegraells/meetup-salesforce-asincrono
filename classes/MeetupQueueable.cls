/**
 * Created by egraells on 31/05/18.
 */

/*
Meetup:

String newJobID = System.enqueueJob(new MeetupQueueable());
//String newJobID = System.enqueueJob(new MeetupBatch([select id from account limit 10], [select id from contact limit 10]));
AsyncApexJob jobInfo = [SELECT Status,NumberOfErrors FROM AsyncApexJob WHERE Id=:newJobID];
System.debug('-ege- Job Info: ' + jobInfo);
System.debug('-ege- De los 50 Maximum number of Apex jobs added to the queue with System.enqueueJob - he consumido: ' + Limits.getQueueableJobs());

Mostrar la Apex Jobs Queue

 */

public with sharing class MeetupQueueable implements Queueable{

    private static decimal milis = 1000;

    //Constructor sin parámetros
    public MeetupQueueable(){

        //Inicialización de lo que sea necesario para el Job
    }

    //Sobrecarga del constructor para recibir parametros y además non-primitive
    public MeetupQueueable(List<Account> listaAccounts, List<Contact> listContactos){

        //Inicialización de lo que sea necesario para el Job
    }

    public void execute(QueueableContext  context) {

        duermete(1*milis);
    }

    private void duermete(decimal milis) {

        Long startTimestamp =  System.currentTimeMillis();
        Long timeDiff = 0;

        DateTime firstTime = System.now();
        do {
            timeDiff = System.now().getTime() - firstTime.getTime();
        } while (timeDiff <= milis);

        Logger.insertLogEntry('Me he echado un siesta de: ' + (System.currentTimeMillis() - startTimestamp)/1000 + ' segundos');

    }

    //Si quisiera realizar un encadenamiento con otro proceso es tan senzillo como:
    //System.enqueueJob(new MeetupBatch);

}