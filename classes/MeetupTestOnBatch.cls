/**
 * Created by egraells on 31/05/18.
 */

@isTest
public with sharing class MeetupTestOnBatch {

    @isTest
    static void TestBatch() {
        System.Test.enqueueBatchJobs(10);

        //Proporciona la lista ordenada de los Jobs en la Test-Context Flex Queue
        for (Id p : System.Test.getFlexQueueOrder()) {

            //Con el identificador accedemos al objeto AsyncApexJobs y obtenemos más información
            for (List<AsyncApexJob> asyncApexJobs : [SELECT Id, Status, JobType FROM AsyncApexJob]) {
                for (AsyncApexJob asyncApexJob : asyncApexJobs) {

                    System.debug('ID:  ' + asyncApexJob.Id
                            + ', JobType:  ' + asyncApexJob.JobType
                            + ', Status:  ' + asyncApexJob.Status
                    );
                }
            }
        }
    }

}