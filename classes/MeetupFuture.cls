/**
 * Created by egraells on 31/05/18.
 */

public with sharing class MeetupFuture {
    /**
 * Created by egraells on 30/05/18.
 */


    private static String endpoint = 'https://th-apex-http-callout.herokuapp.com/animals';

    //Ejecutar: MeetupFuture.compararSyncAsync();

    public static void compararSyncAsync(){

        System.debug('------Sync------');
        System.debug(Datetime.now());
        makeGetCallout();
        System.debug(Datetime.now());

        System.debug('\n\n------Async------');
        System.debug(Datetime.now());
        makeGetCalloutAsync();
        System.debug(Datetime.now());

    }

    //Realizamos una llamada sincrona al Web Service
    public static HttpResponse makeGetCallout() {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {

            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

            // Cast the values in the 'animals' key as a list
            List<Object> animals = (List<Object>) results.get('animals');

            for (Object animal: animals) {
                System.debug(animal);
            }
        }
        return response;
    }

    //Realizamos una llamada asincrona al Web Service + (retoques en la clase)

    @future (callout=true)
    public static void makeGetCalloutAsync() {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {

            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

            // Cast the values in the 'animals' key as a list
            List<Object> animals = (List<Object>) results.get('animals');

            for (Object animal: animals) {
                System.debug(animal);
            }
        }
    }

}